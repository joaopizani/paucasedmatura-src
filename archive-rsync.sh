#!/usr/bin/env bash

DATAROOT_DEFAULT="/data/data"
DATAROOT="$(readlink -m "${1:-"${DATAROOT_DEFAULT}"}")"

ARCHIVEHOST_DEFAULT="hostarchive"
ARCHIVEHOST="${2:-"${ARCHIVEHOST_DEFAULT}"}"

HTDOCS_DEFAULT="/var/www/archive.alvb.in/html"
HTDOCS="${3:-"${HTDOCS_DEFAULT}"}"
REMOTEROOT="${ARCHIVEHOST}:${HTDOCS}"

PRIVNAME_DEFAULT="priv"
PRIVROOT="$(readlink -m "${DATAROOT}/${4:-"${PRIVNAME_DEFAULT}"}")"


rsync -r -l -v "${PRIVROOT}/bsc/material-disciplinas"  "${REMOTEROOT}/bsc/"
rsync -r -l -v "${PRIVROOT}/bsc/TCC"                   "${REMOTEROOT}/bsc/"
rsync -r -l -v "${PRIVROOT}/bsc/curriculo"             "${REMOTEROOT}/bsc/"
rsync -r -l -v "${PRIVROOT}/bsc/paper-icecs2011-hls"   "${REMOTEROOT}/bsc/"

rsync -r -l -v "${PRIVROOT}/bsc-to-msc"  "${REMOTEROOT}/"
rsync -r -l -v "${PRIVROOT}/msc"         "${REMOTEROOT}/"
